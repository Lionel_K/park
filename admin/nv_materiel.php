<?php
	include('inc/connection.inc');
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Park Managment System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- link to style -->
	<link href="css/bootstrap.css" rel="stylesheet" media="screen">
	<link href="css/styles.css" rel="stylesheet" media="screen">
  </head>
  <body>
    
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
	 <script src="js/bootstrap.js"></script>
	    <!-- title -->
	
<?php
	include('inc/title.inc');
?>




    <div class="container-fluid">
    <div class="row-fluid">
    <div class="span3">
    <!-- Menu -->
   



<?php 
include('inc/sidebar.inc');

?>


    <div class="span9">


    <!--Body content-->
<h2 class="text-success text-center">Nouveau mat&eacute;riel</h2>
<!-- form pour enregistrer un nouveaux materiel avec save_materiel.php-->
<form method="post" action="save_materiel.php">
<table align="center">
<tr>
    <td>Direction:</td>
    <td><select name= "Direction">
<option></option>
<?php
// selectioner et afficher tout les departements et services
				$sql=mysql_query("select distinct Direction from Affectation order by Direction asc");
				while($row=mysql_fetch_array($sql)){
			                   echo '<OPTION VALUE=" '.$row['Direction'].'">'.$row['Direction'].'';}
                         ?>
</select></td>
 <td>D&eacute;partement:</td>
    <td><select name= "Nom_affectation">
<option></option>
<?php
// selectioner et afficher tout les departements et services
				$sql=mysql_query("select distinct Nom_affectation from Affectation order by Nom_affectation asc");
				while($row=mysql_fetch_array($sql)){
			                   echo '<OPTION VALUE=" '.$row['Nom_affectation'].'">'.$row['Nom_affectation'].'';}
                         ?>
</select></td>
  </tr>  

<tr>
    <td>Type: </td>
    <td><select name="Type" >
<option></option>
<option>Laptop</option>
<option>Desktop</option>
<option>Imprimante</option>
<option>Scanner</option>
<option>Switch</option>
<option>Routeur</option>
</select></td>
 <td>Etat: </td>
    <td><select name="Etat" >
<option></option>
<option>Neuf</option>
<option>Occasion</option>
<!-- <option>Utilise</option> -->
</select></td>
  </tr>  

<tr>
    <td>Mod&egrave;le:</td>
    <td><input class="input-large" type="text" name="Model" placeholder="Mod&egrave;le ex: HP 620"></td>
     <td>Identification:</td>
    <td><input class="input-large" type="text" name="Num_Serie"  placeholder="Comme le Num&eacute;ro de S&eacute;rie"></td>
  </tr>
<tr>
 
    <td>Syst&egrave;me d'exploitation:</td>
    <td><input class="input-large" type="text" name="operating_system"  placeholder="Syst&egrave;me d'exploitation"></td>
    <td>Capacit&eacute; du disque dur:</td>
    <td><input class="input-large" type="text" name="disk_dur"  placeholder="Ex 500GB"></td>

  </tr>
  <tr>
    <td>Ram:</td>
    <td><input class="input-large" type="text" name="ram"  placeholder="Ex 2Gb"></td>
    <td>Utilisateur:</td>
     <td><select name= "utilisateur">
<option></option>
<?php
// selectioner et afficher tout les departements et services
				$sql=mysql_query("select Username  from Utilisateurs order by Username asc");
				while($row=mysql_fetch_array($sql)){
			                   echo '<OPTION VALUE=" '.$row['Username'].'">'.$row['Username'].'';}
                         ?>
</select></td>

<tr>
    <td>D&eacute;tails:</td>
    <td><textarea rows="3" name="Detail"></textarea></td>
  </tr>
<tr>
    <td></td>
    <td><button class="btn btn-success btn-large" type="submit" name="saving">Enregistrer</button></td>
  </tr></table>   </form> </div>
    </div>
    </div>


    <!-- footer -->
<?php 
include('inc/footer.inc');
?>

  </body>
</html>
