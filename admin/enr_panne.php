<?php
	include('inc/connection.inc');
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Park Managment System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- link to style -->
	<link href="css/bootstrap.css" rel="stylesheet" media="screen">
	<link href="css/styles.css" rel="stylesheet" media="screen">
  </head>
  <body>
    
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
	 <script src="js/bootstrap.js"></script>
	    <!-- title -->
	
<?php
	include('inc/title.inc');
?>
    <div class="container-fluid">
    <div class="row-fluid">
    <div class="span3">
    <!-- Menu -->
   



<?php 
include('inc/sidebar.inc');

?>


    <div class="span9">


    <!--Body content-->
<h2 class="text-success text-center">Enregistrer une nouvelle panne</h2>
<form  method="POST">
<?php
// afficher les ordinateur  et autre materiel existant et n'etant pas en panne
$result = mysql_query("SELECT * FROM Materiel WHERE Etat NOT LIKE 'En Panne'");
echo" <h3 class='text-center'>Liste des Machines Op&eacute;rationelles</h3>";
echo "<table class='table table-striped  table-hover table-bordered'>
<tr class='success'>
<td>Nom</td>
<td>Type</td>
<td>Mod&egrave;le</td>
<td>Affectation</td>
<td>Utilisateur</td>
<td></td>
</tr>";

while($row = mysql_fetch_array($result)) {
  echo "<tr>";
  echo "<td>" . $row['Num_Serie'] . "</td>";
  echo "<td>" . $row['Type'] . "</td>";
  echo "<td>" .$row['Model']. "</td>";
    echo "<td>" .$row['Nom_affectation']. "</td>";
      echo "<td>" .$row['utilisateur']. "</td>";
echo "<td>".' <a href="enr_panne1.php?id='.$row['Num_Serie'].'"  >En panne?</a>'."</td>";
  echo "</tr>";
}

echo "</table>";


?>

<h3 class="text-center">Liste des alertes lanc&eacute;es</h3>


<?php
// afficher les ordinateur  et autre materiel existant avec un alerte lance. la query est base sur les tableau alerte et operation.
$result = mysql_query("SELECT * FROM Alerte INNER JOIN Operation ON Operation.Id_operation = Alerte.Id_alerte AND Fin_operation = ''");
echo "<table class='table table-striped  table-hover table-bordered'>
<tr class='success'>
<td>D&eacute;but de l'op&eacute;ration</td>
<td>Lanc&eacute;e par</td>
<td>Statut</td>
<td>Type</td>
<td></td>
</tr>";

while($row = mysql_fetch_array($result)) {
  echo "<tr>";
  echo "<td>" . $row['Debut_operation'] . "</td>";
  echo "<td>" .$row['Utilisateur_id']. "</td>";
  echo "<td>" .$row['Statut']. "</td>";
echo "<td>".$row['Type']."</td>";
// link basE sur le numero de serie et qui nous ammene a enr_panne2.php pour enregistre les pannes detecter.
echo "<td>".' <a href="enr_panne2.php?id='.$row['Num_Serie'].'"  >Changer</a>'."</td>";
  echo "</tr>";
}
echo "</table>";
?> 

<h3 class="text-center">Liste du mat&eacute;riel en r&eacute;paration</h3>


<?php
// afficher les ordinateur  et autre materiel existant qui sont en reparation. la query est base sur les tableau materiel et operation. elle affiche les materiel avec etat "En Reparation"
$result = mysql_query("SELECT * FROM Operation INNER JOIN Materiel ON Operation.Num_Serie = Materiel.Num_Serie AND Debut_operation NOT LIKE  '%Aler%'
AND Etat LIKE  'En Reparation'
GROUP BY Operation.Num_Serie");
echo "<table class='table table-striped  table-hover table-bordered'>
<tr class='success'>
<td>D&eacute;but de l'op&eacute;ration</td>
<td>Effectu&eacute;e par</td>
<td>Mod&egrave;le</td>
<td>Type</td>

<td></td>
</tr>";

while($row = mysql_fetch_array($result)) {
  echo "<tr>";
  echo "<td>" . $row['Debut_operation'] . "</td>";
  echo "<td>" . $row['User_id'] . "</td>";
  echo "<td>" .$row['Model']. "</td>";
echo "<td>".$row['Type']."</td>";
// link basE sur le numero de serie et qui nous ammene a enr_panne3.php pour enregistre d'autre pannes ou enregistrer le resultat generale des operations
echo "<td>".' <a href="enr_panne3.php?id='.$row['Num_Serie'].'"  >Changer</a>'."</td>";
  echo "</tr>";
}
echo "</table>";
?> 

</form>
    </div>
    </div>
    </div>
    <!-- footer -->
<?php 
include('inc/footer.inc');
?>
  </body>
</html>
