<!DOCTYPE html>
<?php
// start session
session_start();

if (array_key_exists('Utilisateur_id', $_SESSION)) {
    // user already authenticated
    // header('location: site/index.php');
    session_unset();
    session_destroy();
    session_start();
}

if ($_POST) {
  include('admin/inc/connection.inc');

    if (array_key_exists('Utilisateur', $_POST)) {
      $user =  trim($_POST['Utilisateur']);
      $pass = trim($_POST['Password']);
      // $pass = bin2hex(md5($pass, TRUE ));
      $pass = md5($pass);
      echo $pass;
        $sql = "select * from Utilisateurs 
              where Username = '".$user."'";
      // echo $sql;
        $result = mysql_query($sql);
    
        if (mysql_num_rows ($result) ==1) {
          $row = mysql_fetch_assoc($result);
            if ($pass == $row['Password']) {
              // create session variables
                $_SESSION['Utilisateur_id'] = $row['Utilisateur_id'];
                $_SESSION['Username'] = $row['Username'];
                $_SESSION['Nom'] = $row['Nom'];
                $_SESSION['Prenom'] = $row['Prenom'];
                $_SESSION['Matricule'] = $row['Matricule'];
                $_SESSION['Nom_affectation'] = $row['Nom_affectation'];
                $_SESSION['Niveau'] = $row['Niveau'];
		$_SESSION['Direction'] = $row['Direction'];
		$_SESSION['Fonction'] = $row['Fonction'];
		$_SESSION['Password'] = $row['Password'];
                // login successful - redirect to home page
                
                
				if($_SESSION['Niveau'] == 1)
				{
				header("location:users/");
				}
				elseif($_SESSION['Niveau'] == 2)
				{
				header("location:technik/");
				}
				elseif($_SESSION['Niveau'] == 3)
				{
				header('location: admin/');
				}
          
            }
            else
               {
               header("location: bad.php");
               }
    }
    else
      {
         header("location:bad.php");
      }
}
}
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Park Managment System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="admin/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

    </style>
    <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="../assets/ico/favicon.png">
  </head>

  <body>

    <div class="container">

      <form class="form-signin" method="post" action="index.php">
        <h2 class="form-signin-heading text-error">Le login et/ou le mot de passe sont incorrects.</h2>
        <input type="text" class="input-block-level" name="Utilisateur" placeholder="Nom d'utilisateur" required>
        <input type="password" class="input-block-level" name="Password" placeholder="Mot de passe" required>
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Se souvenir de moi
        </label>
        <button class="btn btn-large btn-primary" type="submit">Connexion</button>
      </form>

    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap-transition.js"></script>
    <script src="../assets/js/bootstrap-alert.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-dropdown.js"></script>
    <script src="../assets/js/bootstrap-scrollspy.js"></script>
    <script src="../assets/js/bootstrap-tab.js"></script>
    <script src="../assets/js/bootstrap-tooltip.js"></script>
    <script src="../assets/js/bootstrap-popover.js"></script>
    <script src="../assets/js/bootstrap-button.js"></script>
    <script src="../assets/js/bootstrap-collapse.js"></script>
    <script src="../assets/js/bootstrap-carousel.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script>

  </body>
</html>
