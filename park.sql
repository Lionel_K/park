-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 19, 2014 at 10:41 PM
-- Server version: 5.5.38-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `park`
--

-- --------------------------------------------------------

--
-- Table structure for table `Affectation`
--

CREATE TABLE IF NOT EXISTS `Affectation` (
  `Id_affectation` int(11) NOT NULL AUTO_INCREMENT,
  `Direction` varchar(45) NOT NULL,
  `Nom_affectation` varchar(45) NOT NULL,
  `Nombre_total` int(11) NOT NULL,
  `Nombre_fonctionel` int(11) NOT NULL,
  `Nombre_non_fonctionel` int(11) NOT NULL,
  PRIMARY KEY (`Id_affectation`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Alerte`
--

CREATE TABLE IF NOT EXISTS `Alerte` (
  `Id_alerte` int(11) NOT NULL,
  `Nom_affectation` varchar(45) NOT NULL,
  `Utilisateur_id` varchar(45) NOT NULL,
  `Date_dalerte` varchar(45) NOT NULL,
  `Statut` varchar(45) NOT NULL,
  `Num_Serie` varchar(45) NOT NULL,
  PRIMARY KEY (`Id_alerte`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Materiel`
--

CREATE TABLE IF NOT EXISTS `Materiel` (
  `Num_Serie` varchar(100) NOT NULL,
  `Num_Serie1` varchar(45) NOT NULL,
  `Type` varchar(100) NOT NULL,
  `Model` varchar(100) NOT NULL,
  `Etat` varchar(45) NOT NULL,
  `Date_D_Arrive` varchar(45) NOT NULL,
  `Detail` varchar(45) DEFAULT NULL,
  `Nom_affectation` varchar(45) NOT NULL,
  `Direction` varchar(45) NOT NULL,
  `operating_system` varchar(45) NOT NULL,
  `disk_dur` varchar(45) NOT NULL,
  `ram` varchar(45) NOT NULL,
  `utilisateur` varchar(45) NOT NULL,
  PRIMARY KEY (`Num_Serie`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Operation`
--

CREATE TABLE IF NOT EXISTS `Operation` (
  `Id_operation` int(11) NOT NULL AUTO_INCREMENT,
  `User_id` varchar(45) NOT NULL,
  `Debut_operation` varchar(45) NOT NULL,
  `Fin_operation` varchar(45) NOT NULL,
  `Resultat_final` varchar(45) NOT NULL,
  `Detail_du_resultat` varchar(2000) NOT NULL,
  `Num_Serie` varchar(100) NOT NULL,
  `Type` varchar(45) NOT NULL,
  `Dep_Ser` varchar(45) NOT NULL,
  PRIMARY KEY (`Id_operation`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Operation_details`
--

CREATE TABLE IF NOT EXISTS `Operation_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Id_operation` int(11) NOT NULL,
  `Panne_detectee` varchar(100) NOT NULL,
  `Resultat` varchar(45) NOT NULL,
  `Detail` varchar(2000) NOT NULL,
  `estimation_depence` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Sessions`
--

CREATE TABLE IF NOT EXISTS `Sessions` (
  `Session_id` int(11) NOT NULL,
  `Debut_session` varchar(45) NOT NULL,
  `Fin_session` varchar(45) NOT NULL,
  `Utilisateur_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Session_details`
--

CREATE TABLE IF NOT EXISTS `Session_details` (
  `Session_id` int(11) NOT NULL,
  `Tache` varchar(100) NOT NULL,
  `Heure_D_Action` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Transation`
--

CREATE TABLE IF NOT EXISTS `Transation` (
  `Id_transaction` int(11) NOT NULL AUTO_INCREMENT,
  `Transaction_type` varchar(45) NOT NULL,
  `Date_d_transaction` varchar(45) NOT NULL,
  `Num_serie` varchar(45) NOT NULL,
  `Nom_affectation` varchar(45) NOT NULL,
  `Statut` varchar(45) NOT NULL,
  `Num_recommande` int(11) NOT NULL,
  `Transaction_from` varchar(45) NOT NULL,
  PRIMARY KEY (`Id_transaction`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transfer`
--

CREATE TABLE IF NOT EXISTS `transfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Direction_O` varchar(45) NOT NULL,
  `Direction_N` varchar(45) NOT NULL,
  `Dep_O` varchar(45) NOT NULL,
  `Dep_N` varchar(45) NOT NULL,
  `User_O` varchar(45) NOT NULL,
  `User_N` varchar(45) NOT NULL,
  `Changement_D` varchar(45) NOT NULL,
  `Num_serie` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Utilisateurs`
--

CREATE TABLE IF NOT EXISTS `Utilisateurs` (
  `Utilisateur_id` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(45) NOT NULL,
  `Niveau` int(11) NOT NULL,
  `Password` varchar(2000) NOT NULL,
  `Nom` varchar(45) NOT NULL,
  `Prenom` varchar(45) NOT NULL,
  `Fonction` varchar(45) NOT NULL,
  `Matricule` varchar(45) DEFAULT NULL,
  `Nom_affectation` varchar(45) DEFAULT NULL,
  `Direction` varchar(45) NOT NULL,
  PRIMARY KEY (`Utilisateur_id`),
  UNIQUE KEY `username_UNIQUE` (`Username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `Utilisateurs`
--

INSERT INTO `Utilisateurs` (`Utilisateur_id`, `Username`, `Niveau`, `Password`, `Nom`, `Prenom`, `Fonction`, `Matricule`, `Nom_affectation`, `Direction`) VALUES
(2, 'fghislain', 3, '858e2c41f7592340bf4b75806c2dc6ee', 'Favina', 'Ghislain', 'Stagiaire', '00/09BU', ' Service Informatique', ''),
(3, 'n_mat', 1, '858e2c41f7592340bf4b75806c2dc6ee', 'Ntebutsi', 'Matthias', '', '', ' Service Comptabilite', ''),
(4, 'simbagr', 2, '858e2c41f7592340bf4b75806c2dc6ee', 'Simbananiye', 'Marie Grace', 'Secretaire1', '9889', ' Département Informatique', 'Direction Financière'),
(7, 'gigi', 2, '858e2c41f7592340bf4b75806c2dc6ee', 'Favina', 'Ghislain', 'Stagi', '', ' Département Informatique', ' Direction Financière'),
(8, 'mpawea01', 3, '858e2c41f7592340bf4b75806c2dc6ee', 'Mpawenayo', 'Anselme', 'IT Helpdesk', '2081', ' Département Informatique', ' Direction Financière');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
